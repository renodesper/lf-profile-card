# Profile Card

This project serves as a learning process to learn Flutter. It was created based on Udemy course.

Reference: [https://www.udemy.com/course/flutter-bootcamp-with-dart](https://www.udemy.com/course/flutter-bootcamp-with-dart)

## What you will learn

- How to create Stateless Widgets
- What is the difference between hot reload and hot refresh and running an app from cold
- How to use Containers to lay out your UI
- How to use Columns and Rows to position your UI elements
- How to add custom fonts
- How to add Material icons
- How to style Text widgets
- How to read and use Flutter Documentation

## Screenshot

![Profile Card](docs/screenshot.jpg)
